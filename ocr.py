import re

import cv2
import numpy as np

import pytesseract

from wfm_requests import compute_levenshtein_dist, query_wfm_item

# If you don't have tesseract executable in your PATH, include the following:
pytesseract.pytesseract.tesseract_cmd = r'/usr/bin/tesseract'

def process_patch(patch, LOWER_RANGE, UPPER_RANGE, indx) -> str:

    hsv_img = cv2.cvtColor(patch, cv2.COLOR_BGR2HSV)
    hsv_img_mask = cv2.inRange(hsv_img, LOWER_RANGE, UPPER_RANGE)
    hsv_img_mask = cv2.dilate(
        hsv_img_mask, kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
    patch_processed = cv2.bitwise_and(patch, patch, mask=hsv_img_mask)

    ocr_text_output = pytesseract.image_to_string(
        patch_processed, config=r'--oem 3 --psm 4 --user-words eng.user-words.txt')

    # Only alpha and spaces.
    ocr_text_output = re.sub('[^A-Za-z\ ]+', '', ocr_text_output)
    # Remove any space strings greater than 1, replace with a single space.
    ocr_text_output = re.sub('\ {2,}', ' ', ocr_text_output)
    
    print(ocr_text_output)
    return ocr_text_output
    
def ocr_process_image(img) -> None:

    h, w, _ = img.shape

    w_mult, h_mult = w / 1920, h / 1080

    ITEM_CROP_SIZE = [int(245 * w_mult), int(55 * h_mult)]
    y_coord = int(410 * h_mult)
    ITEM_CROP_COORDS = [
        [int(480 * w_mult), y_coord],
        [int(720 * w_mult), y_coord],
        [int(960 * w_mult), y_coord],
        [int(1210 * w_mult), y_coord]
    ]

    LOWER_RANGE = np.array([0.095*180, 0.111*255, 0.416*255])
    UPPER_RANGE = np.array([0.150*180, 1.00*255, 1.00*255])

    #img_copy = img.copy()
    results = []
    for idx, arr in enumerate(ITEM_CROP_COORDS):

        x_start = int(arr[0])
        y_start = int(arr[1])

        patch_im = img[y_start:y_start+ITEM_CROP_SIZE[1],
                       x_start:x_start+ITEM_CROP_SIZE[0]]
        
        raw_ocr_output = process_patch(patch_im, LOWER_RANGE, UPPER_RANGE, idx)
        matched_item = compute_levenshtein_dist(raw_ocr_output)
        res = query_wfm_item(matched_item)
        if res is not None:
            results.append(res)

    sorted_results = sorted(results)
    for item_res in sorted_results:
        i = item_res.item
        print(f"{i}:\tavg: {item_res.avg_platinum}, volume: {item_res.quantity}, min: {item_res.min_platinum}, max: {item_res.max_platinum}, median: {item_res.med_platinum}, std: {item_res.std_platinum}")


if __name__ == "__main__":

    img = cv2.imread("./images/f1.png")
    h, w, _ = img.shape
    ocr_process_image(img)