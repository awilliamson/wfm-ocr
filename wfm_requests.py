import requests
from dataclasses import dataclass, asdict
from datetime import datetime, timedelta
from typing import List, Optional, Generator, Union
import Levenshtein
import os
import sys
import numpy as np
import json


class WFM_Request_Error(Exception):
    pass

class WFM_Item_Request_Error(Exception):
    pass


@dataclass
class WFM_ITEM:
    item_name: str
    url_name: str

    def __str__(self):
        return self.item_name

    def __iter__(self):
        return self

    def __next__(self):
        return self.item_name, self.url_name


@dataclass
class WFM_Item_Result:
    item: WFM_ITEM
    min_platinum: float
    max_platinum: float
    avg_platinum: float
    med_platinum: float
    std_platinum: float
    quantity: int

    def __lt__(self, other):
        return self.avg_platinum < other.avg_platinum


@dataclass
class WFM_LOOKUP:
    items: List[WFM_ITEM]
    # Default is time + 9 hours.
    expiry: float = (datetime.now() + timedelta(days=1)).timestamp()

    # Always add a forma.
    def __post_init__(self):
        self.__add_forma()

    def __str__(self):
        return f"Expiry: {self.expiry}, Number of Items: {len(self.items)}"

    def __len__(self):
        return len(self.items) - 1 # This feels bad.

    def __add_forma(self) -> None:
        # Add Forma to the item list, as it's a reward but cannot be sold!
        self.items.append(WFM_ITEM("Forma Blueprint", "forma_blueprint"))


BASE_URL = "https://api.warframe.market/v1"
ITEM_CONFIG_FILE = './item_list.json'
CACHE = WFM_LOOKUP([])

def _process_wfm_item_orders(wfm_api_response: dict[str, dict], item: WFM_ITEM) -> WFM_Item_Result:
    # Input is the JSON() response from WFM.
    cutoff = (datetime.now() - timedelta(hours=48)).timestamp()
    filtered_orders = [x for x in wfm_api_response["payload"]["orders"] if datetime.fromisoformat(x["last_update"]).timestamp() >= cutoff]

    quantity = sum([order["quantity"] for order in filtered_orders])
    plat_vals = [order["platinum"] for order in filtered_orders]

    if quantity == 0:
        print(f"No results in filtered_orders, len: {len(filtered_orders)}")
        plat_vals = [0]

    return WFM_Item_Result(
        item = item,
        min_platinum = min(plat_vals),
        max_platinum = max(plat_vals),
        avg_platinum = np.mean(plat_vals),
        med_platinum = np.median(plat_vals),
        std_platinum = np.std(plat_vals),
        quantity = quantity
    )


def query_wfm_item(item: WFM_ITEM) -> Optional[WFM_Item_Result]:
    if item.url_name == "forma_blueprint":
        return None
    endpoint = f"{BASE_URL}/items/{item.url_name}/orders"
    res_obj = requests.request("GET", endpoint)
    if res_obj.ok:
        return _process_wfm_item_orders(res_obj.json(), item)
    else:
        raise WFM_Item_Request_Error(f"Request to {endpoint} resulted in {res_obj.status_code}, {res_obj.reason}. Cannot give Platinum statistics")


# Generator[ yield_type, send_type, return_type ]
def process_items(json_obj) -> Generator[WFM_ITEM, None, None]:
    for entry in json_obj["payload"]["items"]:
        item_name: str = entry["item_name"]
        url_name: str = entry["url_name"]
        yield WFM_ITEM(item_name, url_name)  # type: WFM_ITEM


def request_items() -> WFM_LOOKUP:
    res_obj = requests.request("GET", BASE_URL + "/items")
    if res_obj.ok:
        ret = WFM_LOOKUP([item for item in process_items(res_obj.json())])
        return ret
    else:
        raise WFM_Request_Error(
            f"Request to {BASE_URL + '/items'} resulted in {res_obj.status_code}, {res_obj.reason}. Cannot proceed to building Item Cache.")


def _write_config() -> bool:
    global CACHE
    if CACHE is None:
        return False

    with open(ITEM_CONFIG_FILE, 'w') as fl:
        try:
            data = asdict(CACHE)
            json.dump(data, fl, indent=4)
        except Exception as e:
            print(e)
            return False
        finally:
            fl.close()

    return True


def _read_config() -> Union[WFM_LOOKUP, None]:
    global CACHE
    try:
        with open(ITEM_CONFIG_FILE, 'r') as fl:
            data = json.load(fl)

        # From Dict -> WFM_ITEM
        data["items"] = [WFM_ITEM(**item) for item in data["items"]]
        
        # From Dict (containing WFM_ITEMS) -> WFM_LOOKUP
        CACHE = WFM_LOOKUP(**data)  # Dictionary unpack into the Dataclass
    except Exception as e:
        raise e


def update_item_list() -> WFM_LOOKUP:
    global CACHE  # type: WFM_LOOKUP

    if not len(CACHE) > 0:
        # Check if we have a config file
        if not os.path.exists(ITEM_CONFIG_FILE):
            print("Config file doesn't exist")
            CACHE = request_items()
            # Save it to config.json
            _write_config()
        else:
            _read_config()

    #print(
    #    f"Time Now: {datetime.now().timestamp()}\tTime Expired: {CACHE.expiry}")

    # Technically CACHE should NEVER be empty at this point.
    if len(CACHE) > 0 and CACHE.expiry and datetime.now().timestamp() > CACHE.expiry:  # Expired
        print("Expired Item List, Refreshing")
        CACHE = request_items()
        _write_config()

    return CACHE


def compute_levenshtein_dist(predicted_word: str) -> WFM_ITEM:
    update_item_list() # Ensure up-to-date.

    #print(predicted_word)
    min_dist = sys.maxsize
    for wfm_item in CACHE.items:
        # print(wfm_item)
        # print(wfm_item.item_name)
        dist = Levenshtein.distance(predicted_word, wfm_item.item_name)
        if dist < min_dist:  # Found better match
            best_match = wfm_item
            min_dist = dist  # Update value to beat
            #print(f"Better match: {best_match}, dist: {dist}")

    return best_match # type: ignore


if __name__ == "__main__":
    print(update_item_list())
    compute_levenshtein_dist("Titania Prime NEuasoptics")
