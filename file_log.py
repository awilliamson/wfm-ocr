import sys
import time

import cv2
import gevent
import numpy as np
import psutil
import toml
from Xlib import X, display
from Xlib.protocol.rq import Window

from ocr import ocr_process_image
from utils import get_warframe_window

try:
    from PIL import Image
except ImportError:
    import Image

from gevent import monkey

monkey.patch_all()

gotRewardsQueue = gevent.queue.Queue()

class CfgError(Exception):
    pass

def check_logs(p: str) -> None:
    with open(p, "r", encoding="utf-8") as log_file:
        try:
            while True:
                line = log_file.readline()
                if "Got rewards" in line:
                    gotRewardsQueue.put(True)
                else:
                    gevent.sleep(sys.float_info.epsilon)
        except Exception as e:
            print(e)


def take_screenshot_and_process(save_screenshot: bool = True):
    dsp = display.Display()
    try:
        root = dsp.screen().root
        query = root.query_tree()

        c = get_warframe_window(query, "Warframe")  # type: Window

        while c.get_wm_state().state != 1:
            print("Waiting for Warframe to be active...")
            gevent.sleep(0.1)

        print("Warframe Active, taking screenshot")
        geo = c.get_geometry()._data
        x, y, W, H = geo['x'], geo['y'], geo['width'], geo['height']
        raw = c.get_image(x, y, W, H, X.ZPixmap, 0xffffffff)
        image = Image.frombytes("RGB", (W, H), raw.data, "raw", "BGRX")
        rgb_image = np.array(image)[:, :, ::-1]

        ocr_process_image(rgb_image)

        if save_screenshot:
            # Reorder channels, as it comes out as BGR.
            cv2.imwrite(
                f"images/in-game/screenshot_{time.time()}.png", np.array(image)[:, :, ::-1])
    finally:
        dsp.close()


def ocr_queue_processor() -> None:
    while True:
        if gotRewardsQueue.get():
            take_screenshot_and_process()

if __name__ == "__main__":
        
    try:
        cfg = toml.load('./config.toml')
    except TypeError as typ_err:
        raise typ_err
    except toml.TomlDecodeError as toml_err:
        raise toml_err
    except [IOError, FileNotFoundError] as io_err:
        raise io_err

    try:
        warframe_pid = None
        print("Waiting on Warframe to start")

        while warframe_pid is None:
            # TODO Make more robust. Catch if it's the launcher, or the actual game!
            # As the launcher exits prior to game launch, crashing out.
            for pid in psutil.pids():
                p = psutil.Process(pid)
                if "Warframe" in p.name():
                    warframe_pid = pid
                    print(f"Warframe PID: {pid}")
            time.sleep(5)

        glets = []

        try:
            cfg_path = cfg['path']
        except KeyError as key_err:
            raise CfgError("Path cannot be obtained from config.json")

        log_checker = gevent.Greenlet(
            check_logs, cfg['path'])  # type gevent.Greenlet
        ocr_thing = gevent.Greenlet(ocr_queue_processor)

        glets.append(log_checker)
        glets.append(ocr_thing)

        for glet in glets:
            glet.start()

        while psutil.pid_exists(warframe_pid):
            time.sleep(5)

        print(f"Warframe Closed, cleaning up")
        gevent.killall(glets)
        for glet in glets:
            print(f"Killed the glet: {glet.dead}")

    except Exception as e:
        print(e)
