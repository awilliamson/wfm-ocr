from Xlib.protocol.rq import Window

def get_warframe_window(node: Window, target: str) -> Window:
    for c in node.children:
        cls = c.get_wm_class()
        name = c.get_wm_name()

        if cls and name:

            if name != target:
                rtn = get_warframe_window(c.query_tree(), target)
                if rtn is None:
                    continue
                else:
                    return rtn

            return c
